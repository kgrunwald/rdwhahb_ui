/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.widgets')
        .directive('selectpicker', selectpicker);

    /** @ngInject */
    function selectpicker() {
        return {
            restrict: 'A',
            require: '?ngOptions',
            priority: 1500, // make priority bigger than ngOptions and ngRepeat
            link: {
                pre: function (scope, elem, attrs) {
                    elem.append('<option data-hidden="true" disabled value="">' + (attrs.title || 'Select something') + '</option>')
                },
                post: function (scope, elem, attrs) {
                    function refresh() {
                        elem.selectpicker('refresh');
                    }

                    if (attrs.ngModel) {
                        scope.$watch(attrs.ngModel, refresh);
                    }

                    if (attrs.ngDisabled) {
                        scope.$watch(attrs.ngDisabled, refresh);
                    }

                    if (attrs['options']) {
                        scope.$watch(attrs['options'], function (value) {
                            if (value) {
                                scope.$applyAsync(function () {
                                    elem.selectpicker('refresh');
                                });
                            }
                        }, true);
                    }

                    elem.selectpicker({dropupAuto: false, hideDisabled: true});

                }
            }
        };
    }


})();