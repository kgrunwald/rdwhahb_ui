/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.widgets')
        .directive('switch', switchDirective);

    /** @ngInject */
    function switchDirective() {
        return {
            restrict: 'AE',
            replace: true,
            scope: {
                controls: '=',
                color: '=',
                switchDisabled: '@'
            },
            template: '<input type="checkbox" state="controls.state">',
            controller: 'SwitchCtrl'
        };
    }
})();