/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.widgets')
        .controller('SwitchCtrl', SwitchCtrl);

    /** @ngInject */
    function SwitchCtrl($scope, $element) {

        $scope.controls.override = false;
        $scope.controls.state = false;

        $($element).bootstrapSwitch({
            size: 'small',
            onColor: $scope.color
        });

        $($element).on('switchChange.bootstrapSwitch', function (event, state) {
            if ($scope.controls.state != state) {
                $scope.controls.state = state;
                $scope.$apply();
            }
        });

        $scope.$watch("color", function () {
            $($element).bootstrapSwitch('onColor', $scope.color);
        });

        $scope.$watch("controls.state", function () {
            var override = $($element).bootstrapSwitch('disabled');
            $($element).bootstrapSwitch('disabled', false);
            $($element).bootstrapSwitch('state', $scope.controls.state);
            $($element).bootstrapSwitch('disabled', override);
            $($element).bootstrapSwitch('disabled', override);
        });

        $scope.$watch("switchDisabled", function () {
            $($element).bootstrapSwitch('disabled', !$scope.$eval($scope.switchDisabled));
        });
    }
})();