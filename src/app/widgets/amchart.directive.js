'use strict';
angular.module('BlurAdmin.widgets', [])
    .directive('amchart', function () {
        return {
            replace: true,
            scope: {
                options: '=ngModel',
                zoom: '=',
                dates: '='
            },
            template: "<div class='amchart' style='width: 100%; height: 100%;'></div>",
            controller: function ($scope, $element, $timeout) {
                var guid = function guid() {
                    function s4() {
                        return Math.floor((1 + Math.random()) * 0x10000)
                            .toString(16)
                            .substring(1);
                    }

                    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                        s4() + '-' + s4() + s4() + s4();
                };

                var id = guid();
                $element.attr('id', id);
                $scope.chart = null;

                if ($scope.options) {
                    //Função que renderiza o gráfico na tela
                    var renderChart = function (amChartOptions) {
                        var option = amChartOptions || $scope.options;

                        //verificando qual tipo é o gráfico
                        switch (option.type) {
                            case "serial":
                                $scope.chart = new AmCharts.AmSerialChart();
                                break;
                            case "pie":
                                $scope.chart = new AmCharts.AmPieChart();
                                break;
                            case "funnel":
                                $scope.chart = new AmCharts.AmFunnelChart();
                                break;
                            case "gauge":
                                $scope.chart = new AmCharts.AmAngularGauge();
                                break;
                            case "radar":
                                $scope.chart = new AmCharts.AmRadarChart();
                                break;
                            case "xy":
                                $scope.chart = new AmCharts.AmXYChart();
                                break;
                        }


                        $scope.chart.dataProvider = option.data;

                        var chartKeys = Object.keys(option);
                        for (var i = 0; i < chartKeys.length; i++) {
                            if (typeof option[chartKeys[i]] !== 'object' && typeof option[chartKeys[i]] !== 'function') {
                                $scope.chart[chartKeys[i]] = option[chartKeys[i]];
                            } else {
                                $scope.chart[chartKeys[i]] = angular.copy(option[chartKeys[i]]);
                            }
                        }
                        $scope.chart.write(id);
                        $scope.ignoreZoomed = false;

                        $scope.chart.addListener("zoomed", function (event) {
                            if ($scope.ignoreZoomed) {
                                $scope.ignoreZoomed = false;
                                return;
                            }
                            $timeout(function () {
                                $scope.zoom.zoomStartDate = event.startDate;
                                $scope.zoom.zoomEndDate = event.endDate;
                            });
                        });

                        $scope.chart.addListener("dataUpdated", function () {
                            if (!$scope.dates.startDate && $scope.zoom.zoomStartDate || ($scope.zoom.zoomStartDate &&
                                moment($scope.zoom.zoomStartDate)
                                    .isBetween($scope.dates.startDate, $scope.dates.endDate) &&
                                moment($scope.zoom.zoomEndDate)
                                    .isBetween($scope.dates.startDate, $scope.dates.endDate))) {
                                $scope.chart.zoomToDates($scope.zoom.zoomStartDate, $scope.zoom.zoomEndDate);
                                return
                            }
                            $scope.zoom.zoomStartDate = null
                            $scope.zoom.zoomEndDate = null
                        }, {passive: true});
                    };

                    renderChart();


                }


                $scope.$watch('options.data', function () {
                    $scope.chart.dataProvider = $scope.options.data;
                    $scope.ignoreZoomed = true;
                    $scope.chart.validateData();
                }, true);

                $scope.$watch('zoom.zoomStartDate', function () {
                    if ($scope.zoom.zoomStartDate) {
                        $scope.chart.zoomToDates($scope.zoom.zoomStartDate, $scope.zoom.zoomEndDate);
                    }
                });

            }
        };
    });