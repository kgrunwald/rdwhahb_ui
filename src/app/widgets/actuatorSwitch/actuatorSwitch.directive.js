/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.widgets')
        .directive('actuatorSwitch', actuatorSwitch);

    /** @ngInject */
    function actuatorSwitch() {
        return {
            restrict: 'EA',
            scope: {
                actuatorId: "=",
                controls: "=",
                switchColor: "=",
            },
            templateUrl: 'app/widgets/actuatorSwitch/actuatorSwitch.html',
        };
    }
})();