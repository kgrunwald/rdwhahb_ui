/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.widgets')
        .directive('login', login);

    /** @ngInject */
    function login() {
        return {
            restrict: 'EA',
            scope: {
                actuatorId: "=",
                controls: "=",
                switchColor: "=",
            },
            templateUrl: 'app/widgets/login/login.html',
            controller: 'LoginCtrl',
        }
    }
})();