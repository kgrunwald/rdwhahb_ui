/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.widgets')
        .controller('LoginCtrl', LoginCtrl);

    /** @ngInject */
    function LoginCtrl(AuthenticationService, $state, ToastService, $scope) {

        $scope.login = function login() {
            $scope.dataLoading = true;
            AuthenticationService.Login($scope.email, $scope.password, function (response) {
                if (response && response.success) {
                    AuthenticationService.SetCredentials(response.data);
                    $scope.dataLoading = false;
                    $state.go($state.current, {}, {reload: true})
                        .then(function () {
                            $('.al-user-profile').click();
                        })
                } else {
                    ToastService.Open('error', 'Login Failed', 'Email or password was incorrect')
                }
                $scope.dataLoading = false;
            });
        };
    }
})();