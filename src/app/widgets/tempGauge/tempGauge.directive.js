/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.widgets')
        .directive('tempGauge', tempGauge);

    /** @ngInject */
    function tempGauge() {
        return {
            restrict: 'EA',
            scope: {
                actuatorOn: "=",
                actuatorColor: "=",
                data: "="
            },
            controller: 'TempGaugeCtrl',
            templateUrl: 'app/widgets/tempGauge/tempGauge.html'
        };
    }
})();