/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.widgets')
        .controller('TempGaugeCtrl', TempGaugeCtrl);

    /** @ngInject */
    function TempGaugeCtrl($scope, $timeout, $element, baConfig, baUtil) {

        var innerChart;
        var outerChart;

        $scope.data.tempValue = 0;
        $scope.data.setValue = 0;
        $scope.data.minValue = 0;
        $scope.data.maxValue = 100;

        $scope.chart = {
            outerColor: baUtil.hexToRGB(baConfig.colors.success, 1),
            innerColor: baUtil.hexToRGB(baConfig.colors.defaultText, 1),
            icon: 'temperature',
            setPercent: 0,
            tempPercent: 0
        }

        function getPercentFromValue(value, max, min) {
            var diff = max - min
            var percent = ((100 * value) / diff) - ((100 * min) / diff);
            return percent ? percent : 0
        }

        function getValueFromPercent(value, max, min) {
            var diff = max - min;
            var percent = ((value + (100 * min) / diff) * diff) / 100;
            return percent ? percent : 0
        }

        function loadPieCharts() {

            outerChart = $($element).find('.outer-chart');
            outerChart.easyPieChart({
                easing: 'defaultEasing',
                onStep: function (from, to, percent) {
                    var value = Math.round(getValueFromPercent(percent, $scope.data.maxValue, $scope.data.minValue) * 10) / 10
                    outerChart.find('.outer-percent').text(value);
                },
                barColor: outerChart.attr('outer-color'),
                trackColor: 'rgba(0,0,0,0)',
                size: 300,
                scaleLength: 20,
                scaleColor: 'rgba(192,192,192,192)',
                animation: 2000,
                lineWidth: 20,
                lineCap: 'square',
                rotate: 180,
            });

            innerChart = $($element).find('.inner-chart');
            innerChart.easyPieChart({
                easing: 'defaultEasing',
                onStep: function (from, to, percent) {
                    var value = Math.round(getValueFromPercent(percent, $scope.data.maxValue, $scope.data.minValue) * 10) / 10
                    innerChart.find('.inner-percent').text(value);
                },
                barColor: innerChart.attr('inner-color'),
                trackColor: 'rgba(0,0,0,0)',
                size: 300,
                scaleLength: 30,
                scaleColor: 'rgba(0,0,0,0)',
                animation: 2000,
                lineWidth: 20,
                lineCap: 'square',
                rotate: 180,
            });

            innerChart.find('.outer-percent')[0].style.color = baConfig.colors.success;
            $(outerChart).data('easyPieChart').options.barColor = baConfig.colors.success;

            updateData();
        }

        function updateData() {

            $scope.chart.tempPercent = getPercentFromValue($scope.data.tempValue, $scope.data.maxValue, $scope.data.minValue)
            $scope.chart.setPercent = getPercentFromValue($scope.data.setValue, $scope.data.maxValue, $scope.data.minValue)

            if ($scope.data.threshold) {
                if (Math.abs($scope.data.tempValue - $scope.data.setValue) > $scope.data.threshold) {
                    innerChart.find('.outer-percent')[0].style.color = baConfig.colors.warning;
                    $(outerChart).data('easyPieChart').options.barColor = baConfig.colors.warning;
                }
            }

            $(outerChart).data('easyPieChart').update($scope.chart.tempPercent);
            $(innerChart).data('easyPieChart').update($scope.chart.setPercent);

        }

        $timeout(function () {
            loadPieCharts();
        }, 0);

        $scope.$watch('data.tempValue', function () {
            if (!($(outerChart).data('easyPieChart') || $(innerChart).data('easyPieChart'))) {
                return
            }
            updateData();
        });

        $scope.$watch('data.setValue', function () {
            if (!($(outerChart).data('easyPieChart') || $(innerChart).data('easyPieChart'))) {
                return
            }
            updateData();
        });

        $scope.$watch('actuatorOn', function () {
            if (!($(outerChart).data('easyPieChart') || $(innerChart).data('easyPieChart'))) {
                return
            }
            updateData();
        });

        $scope.$watch('actuatorColor', function () {
            if ($(outerChart).data('easyPieChart') && innerChart) {
                innerChart.find('.outer-percent')[0].style.color = baConfig.colors[$scope.actuatorColor];
                $(outerChart).data('easyPieChart').options.barColor = baConfig.colors[$scope.actuatorColor];
                $(outerChart).data('easyPieChart').update($scope.chart.tempPercent);
            }
        });

    }
})();