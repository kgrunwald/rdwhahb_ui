/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.charts', [
        'BlurAdmin.widgets.charts.chartJs',
        'BlurAdmin.widgets.charts.chartist',
        'BlurAdmin.widgets.charts.morris',
        'BlurAdmin.widgets.charts.amCharts',
    ])

})();
