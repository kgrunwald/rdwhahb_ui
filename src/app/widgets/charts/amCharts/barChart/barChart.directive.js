/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.widgets.charts.amCharts')
        .directive('barChart', barChart);

    /** @ngInject */
    function barChart() {
        return {
            restrict: 'EA',
            scope: {
                graphData: "=",
            },
            templateUrl: 'app/widgets/charts/amCharts/barChart/barChart.html',
            controller: 'BarAreaCtrl',
        }
    }
})();