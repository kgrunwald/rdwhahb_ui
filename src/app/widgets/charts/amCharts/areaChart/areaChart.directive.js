/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.widgets.charts.amCharts')
        .directive('areaChart', areaChart);

    /** @ngInject */
    function areaChart() {
        return {
            restrict: 'EA',
            scope: {
                graphData: "=",
                apiId: "="
            },
            template: '<div id="thisChart" class="admin-chart"></div>',
            controller: 'AreaChartCtrl',
        }
    }
})();