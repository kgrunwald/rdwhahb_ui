/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.brews')
        .controller('BrewCtrl', BrewCtrl);

    /** @ngInject */
    function BrewCtrl($scope, $timeout, $interval, $state, $q, $rootScope, BrewService,
                      ActuatorService, SensorService, baConfig, layoutPaths, ToastService) {
        var layoutColors = baConfig.colors;
        $scope.layoutColors = layoutColors;
        // store the interval promise in this variable

        $scope.updating = true;

        $scope.coolColor = 'info';
        $scope.heatColor = 'danger';
        $scope.warnColor = 'warning';
        $scope.goodColor = 'success';
        $scope.brewColor = 'success';
        $scope.tempColor = 'success';

        $scope.formData = {};
        $scope.innerData = {};
        $scope.brewData = {};
        $scope.coolerControls = {};
        $scope.heaterControls = {};

        $scope.brewTempData = [[]];
        $scope.brewTempLabels = [];

        $scope.zoom = {
            zoomStartDate: null,
            zoomEndDate: null
        };

        $scope.chartDates = {
            startDate: '',
            endDate: ''
        };

        $scope.maxDate = moment().valueOf();

        $scope.moment = moment;

        $scope.showPhases = false;

        $scope.coolerChartOptions = {
            type: 'serial',
            theme: 'blur',
            height: 325,
            maxHeight: 325,
            minHeight: 100,
            color: layoutColors.defaultText,
            dataProvider: $scope.chartData,
            balloon: {
                cornerRadius: 6,
                horizontalPadding: 15,
                verticalPadding: 10
            },
            valueAxes: [
                {
                    temperature: 'mm',
                    gridAlpha: 0.5,
                    gridColor: layoutColors.border,
                    minMaxMultiplier: 1.2,
                    minimum: 35,
                    maximum: 80
                }
            ],
            graphs: [
                {
                    id: 'g1',
                    bullet: 'none',
                    bulletBorderAlpha: 1,
                    bulletBorderThickness: 1,
                    fillAlphas: 0.5,
                    fillColorsField: 'lineColor',
                    balloonText: '[[value]]°',
                    lineColorField: 'lineColor',
                    title: 'temperature',
                    valueField: 'temperature',
                    type: 'smoothedLine'
                }
            ],
            chartScrollbar: {
                graph: 'g1',
                scrollbarHeight: 30
            },
            chartCursor: {
                cursorPosition: "mouse",
                categoryBalloonDateFormat: 'YYYY-MM-DD JJ:NN:SS',
                valueBalloonText: '([[value]]°)',
                cursorAlpha: 1,
                fullWidth: true
            },
            dataDateFormat: 'YYYY-MM-DD JJ:NN:SS',
            categoryField: 'date',
            categoryAxis: {
                parseDates: true,
                minPeriod: 'ss',
                autoGridCount: true,
                gridAlpha: 0.5,
                gridColor: layoutColors.border,
                minorGridEnabled: true

            },
            responsive: {
                enabled: false
            },
            export: {
                enabled: true
            },
            pathToImages: layoutPaths.images.amChart
        };

        $scope.brewChartOptions = {
            type: 'serial',
            theme: 'blur',
            height: 325,
            maxHeight: 325,
            minHeight: 100,
            color: layoutColors.defaultText,
            dataProvider: $scope.chartData,
            balloon: {
                cornerRadius: 6,
                horizontalPadding: 15,
                verticalPadding: 10
            },
            valueAxes: [
                {
                    temperature: 'mm',
                    gridAlpha: 0.5,
                    gridColor: layoutColors.border,
                    minMaxMultiplier: 1.2,
                    minimum: 35,
                    maximum: 80
                }
            ],
            graphs: [
                {
                    id: 'g1',
                    bullet: 'none',
                    bulletBorderAlpha: 1,
                    bulletBorderThickness: 1,
                    fillAlphas: 0.5,
                    fillColorsField: 'lineColor',
                    balloonText: '[[value]]°',
                    lineColorField: 'lineColor',
                    title: 'temperature',
                    valueField: 'temperature',
                    type: 'smoothedLine'
                },
                {
                    id: "g2",
                    valueAxis: "v2",
                    color: layoutColors.defaultText,
                    bullet: "round",
                    bulletBorderAlpha: 1,
                    bulletColor: layoutColors.defaultText,
                    bulletSize: 5,
                    hideBulletsCount: 50,
                    lineThickness: 2,
                    lineColor: layoutColors.danger,
                    title: "Expected Value",
                    useLineColorForBulletBorder: true,
                    valueField: "expected",
                    balloonText: '[[title]]<br/><b style=\'font-size: 130%\'>[[value]]°</b>'
                }
            ],
            chartScrollbar: {
                graph: 'g1',
                scrollbarHeight: 30
            },
            chartCursor: {
                cursorPosition: "mouse",
                categoryBalloonDateFormat: 'YYYY-MM-DD JJ:NN:SS',
                valueBalloonText: '([[value]]%)',
                cursorAlpha: 1,
                fullWidth: true
            },
            dataDateFormat: 'YYYY-MM-DD JJ:NN:SS',
            categoryField: 'date',
            categoryAxis: {
                parseDates: true,
                minPeriod: 'ss',
                autoGridCount: true,
                gridAlpha: 0.5,
                gridColor: layoutColors.border,
                minorGridEnabled: true

            },
            responsive: {
                enabled: false
            },
            export: {
                enabled: false
            },
            pathToImages: layoutPaths.images.amChart
        };

        $timeout(function () {
            $scope.updating = false;
        }, 0);

        $scope.getRemainingControllerTime = function () {
            var remaining = $scope.controllerCompleteDate - moment().valueOf();
            if (remaining < 0) {
                return 0
            }
            return remaining;
        };

        $scope.getRemainingControllerPercent = function () {
            var percent = Math.floor((1 - ($scope.getRemainingControllerTime() / $scope.getTotalControllerTime())) * 100);
            return percent && percent > 0 ? percent : 0;
        };

        $scope.getTotalControllerTime = function () {
            return $scope.controllerCompleteDate - $scope.controllerStartDate
        };

        $scope.getExpectedValue = function (date) {
            var currentTime = moment(date);
            var diffMinutes = moment.duration(moment(currentTime).diff(moment($scope.controllerStartDate))).asMinutes();
            var calcMinutes = 0;
            if ($scope.schedule.phases) {
                for (var i = 0; i < $scope.schedule.phases.length; i++) {
                    calcMinutes = calcMinutes + $scope.schedule.phases[i].duration;
                    if (diffMinutes >= 0 && diffMinutes < calcMinutes) {
                        return $scope.schedule.phases[i].setPoint
                    }
                }
            }
            return false
        };

        $scope.getCurrentPhaseIndex = function () {
            if ($scope.schedule) {
                var diffMinutes = moment.duration(moment().diff(moment($scope.controllerStartDate))).asMinutes();
                var calcMinutes = 0;
                for (var i = 0; i < $scope.schedule.phases.length; i++) {
                    calcMinutes = calcMinutes + $scope.schedule.phases[i].duration;
                    if (diffMinutes >= 0 && diffMinutes < calcMinutes) {
                        return i
                    }
                }
            }
            return null
        };

        $scope.getPhaseCompleteDate = function () {
            var totalPhase = 0;
            if ($scope.schedule) {
                for (var i = 0; i <= $scope.currentPhaseIndex; i++) {
                    totalPhase = totalPhase + $scope.schedule.phases[i].duration
                }
            }
            return moment($scope.controllerStartDate).add(totalPhase, 'minutes')
        };

        $scope.getRemainingPhaseTime = function () {
            var remaining = $scope.phaseCompleteDate - moment().valueOf();
            if (remaining < 0) {
                return 0
            }
            return remaining;
        };

        $scope.getTotalPhaseTime = function () {
            if ($scope.currentPhaseIndex !== null) {
                return moment.duration($scope.schedule.phases[$scope.currentPhaseIndex].duration, 'minutes').valueOf()
            }
            return 0
        };

        $scope.getRemainingPhasePercent = function () {
            var percent = Math.floor((1 - ($scope.getRemainingPhaseTime() / $scope.totalPhaseTime)) * 100)
            return percent && percent > 0 ? percent : 0;
        }

        function updateChart(id, tempChartData) {

            $scope.chartMax = 0;
            $scope.chartMin = 1000;

            if (!($scope.coolerId || $scope.heaterId || id)) {
                return
            }

            var found = false;
            var tempData = [];
            var coolerState = [];
            var heaterState = [];
            var initCoolerState = false;
            var initHeaterState = false;
            var states = {};
            states[$scope.coolerId] = false;
            states[$scope.heaterId] = false;

            var endDate = $scope.chartDates.endDate;
            if ($scope.chartDates.endDate) {
                endDate = moment($scope.chartDates.endDate).toString()
            } else {
                endDate = moment().toString()
            }

            var startDate = $scope.chartDates.startDate;
            if ($scope.chartDates.startDate) {
                startDate = moment($scope.chartDates.startDate).toString()
            } else {
                startDate = moment(endDate).subtract(6, 'hours').toString()
            }

            var initStartDate = moment(startDate).subtract(5, 'days').toString();
            var combined = [];

            $q.props({
                tempData: SensorService.GetHistory(id, {
                    startTime: startDate,
                    endTime: endDate
                }),
                coolerState: ActuatorService.GetHistory($scope.coolerId, {
                    startTime: startDate,
                    endTime: endDate
                }),
                heaterState: ActuatorService.GetHistory($scope.heaterId, {
                    startTime: startDate,
                    endTime: endDate
                }),
                initCoolerState: ActuatorService.GetHistory($scope.coolerId, {
                    startTime: initStartDate,
                    endTime: startDate,
                    order: 'DESC'
                }),
                initHeaterState: ActuatorService.GetHistory($scope.heaterId, {
                    startTime: initStartDate,
                    endTime: startDate,
                    order: 'DESC'
                })
            }).then(function (results) {

                tempData = results.tempData.data;
                coolerState = results.coolerState.data;
                heaterState = results.heaterState.data;

                combined = tempData.concat(coolerState, heaterState);

                if (results.initCoolerState.data[0]) {
                    results.initCoolerState.data[0].timestamp = moment(startDate).format("YYYY-MM-DDTHH:mm:ss")
                    combined.push(results.initCoolerState.data[0]);
                    initCoolerState = results.initCoolerState.data[0].value;
                }
                if (results.initHeaterState.data[0]) {
                    results.initHeaterState.data[0].timestamp = moment(startDate).format("YYYY-MM-DDTHH:mm:ss")
                    combined.push(results.initHeaterState.data[0]);
                    initHeaterState = results.initHeaterState.data[0].value;
                }
                states.cooler = initCoolerState;
                states.heater = initHeaterState;

                combined = combined.map(function (item) {
                    item.moment = moment(item.timestamp);
                    return item;
                });

                combined.sort(function (a, b) {
                    if (a.moment.isBefore(b.moment)) {
                        return -1
                    } else if (b.moment.isBefore(a.moment)) {
                        return 1
                    } else {
                        return 0;
                    }
                });

                return combined

            }).reduce(function (memo, item, index) {

                if (combined.length > 100 && index % Math.floor(combined.length/100)) {
                    return memo;
                }

                var data = {
                    date: item.timestamp,
                    temperature: item.value
                };

                if (parseInt(item.value) < $scope.chartMin) {
                    $scope.chartMin = item.value;
                }

                if (parseInt(item.value) > $scope.chartMax) {
                    $scope.chartMax = item.value;
                }


                var expectedValue = $scope.getExpectedValue(item.timestamp);

                if (expectedValue) {
                    data.expected = expectedValue
                }

                if (item.hasOwnProperty('actuatorId') && memo && memo[memo.length - 1]) {
                    states[item.actuatorId] = item.state;
                    found = true;
                    data.temperature = memo[memo.length - 1].temperature
                }
                if (found || !(index % 1)) {
                    found = false;
                    if (states[$scope.coolerId]) {
                        if (states[$scope.heaterId]) {
                            data.lineColor = layoutColors.warning
                        } else {
                            data.lineColor = layoutColors.info
                        }
                    } else {
                        if (states[$scope.heaterId]) {
                            data.lineColor = layoutColors.danger
                        } else {
                            data.lineColor = layoutColors.success
                        }
                    }
                    memo.push(data);
                }
                return memo

            }, []).then(function (items) {

                if (initCoolerState) {
                    if (initHeaterState) {
                        items[0].lineColor = layoutColors.warning;
                    } else {
                        items[0].lineColor = layoutColors.info;
                    }
                } else if (initHeaterState) {
                    if (initCoolerState) {
                        items[0].lineColor = layoutColors.warning;
                    } else {
                        items[0].lineColor = layoutColors.danger;
                    }
                } else {
                    items[0].lineColor = layoutColors.success;
                }
                console.log('update');
                console.log(items.length);
                tempChartData.data = items;
            }).catch(function (err) {
                console.log(err);
            });

        }

        function updateData() {

            BrewService.GetById($state.current.id)
                .then(function (response) {

                    var brews = response.data;

                    $scope.updating = true;

                    $scope.maxDate = moment().valueOf();
                    $scope.innerData.minValue = brews.cooler.sensor.minValue;
                    $scope.innerData.maxValue = brews.cooler.sensor.maxValue;
                    $scope.innerData.tempValue = brews.cooler.sensor.value;
                    $scope.innerData.setValue = brews.cooler.setPoint;
                    $scope.innerData.id = brews.cooler.sensor.id;
                    $scope.innerData.threshold = null;
                    $scope.coolerId = brews.cooler.actuator.id;
                    $scope.coolerControls.state = brews.cooler.actuator.state;
                    $scope.coolerControls.override = brews.cooler.actuator.override;
                    $scope.brewId = brews.id;
                    $scope.controllers = brews.controllers;

                    var found = false;
                    for (var i = 0; i < $scope.controllers.length; i++) {
                        if ($scope.controllers[i].active) {
                            found = true;
                            $scope.controllerStartDate = moment($scope.controllers[i].startTime).valueOf();
                            $scope.controllerCompleteDate = moment($scope.controllers[i].projectedEndTime).valueOf();
                            $scope.controllerId = $scope.controllers[i].id;
                            $scope.brewData.id = $scope.controllers[i].sensor.id;
                            $scope.brewData.minValue = $scope.controllers[i].sensor.minValue;
                            $scope.brewData.maxValue = $scope.controllers[i].sensor.maxValue;
                            $scope.brewData.tempValue = $scope.controllers[i].sensor.value;
                            $scope.brewData.setValue = $scope.controllers[i].currentSetPoint;
                            $scope.brewData.threshold = $scope.controllers[i].currentAcceptableDrift;
                            $scope.heaterId = $scope.controllers[i].actuator.id;
                            $scope.heaterControls.state = $scope.controllers[i].actuator.state;
                            $scope.heaterControls.override = $scope.controllers[i].actuator.override;
                            $scope.active = $scope.controllers[i].active;
                            $scope.complete = $scope.controllers[i].complete;
                            $scope.schedule = $scope.controllers[i].schedule;
                            $scope.controllerType = $scope.controllers[i].type;
                            $scope.currentPhaseIndex = $scope.getCurrentPhaseIndex();
                            $scope.phaseCompleteDate = $scope.getPhaseCompleteDate();
                            $scope.totalPhaseTime = $scope.getTotalPhaseTime();
                            setOuterGaugeColor();
                        }
                    }

                    if (!found) {
                        $scope.active = false;
                        $scope.controllerId = null;
                        $scope.brewData = {};
                        $scope.innerData = {};
                        $scope.coolerControls = {};
                        $scope.heaterControls = {};
                        $scope.schedule = [];
                    }

                    $timeout(function () {
                        $scope.updating = false;
                        updateChart($scope.innerData.id, $scope.coolerChartOptions);
                        updateChart($scope.brewData.id, $scope.brewChartOptions);
                    }, 1000)
                });


        }

        function setOuterGaugeColor() {
            if ($scope.coolerControls.state && $scope.heaterControls.state) {
                $rootScope.setBackgroundColor($scope.layoutColors[$scope.warnColor]);
                $scope.tempColor = $scope.warnColor;
            } else if ($scope.coolerControls.state) {
                $rootScope.setBackgroundColor($scope.layoutColors[$scope.coolColor]);
                $scope.tempColor = $scope.coolColor;
            } else if ($scope.heaterControls.state) {
                $rootScope.setBackgroundColor($scope.layoutColors[$scope.heatColor]);
                $scope.tempColor = $scope.heatColor;
            } else {
                $rootScope.setBackgroundColor();
                $scope.tempColor = $scope.goodColor;
            }
        }

        $scope.startController = function (controllerId) {
            BrewService.StartController($scope.brewId, controllerId)
                .catch(function (response) {
                    ToastService.ErrorFromResponse(response)
                })
                .finally(updateData)
        };

        $scope.stopController = function () {
            BrewService.StopController($scope.brewId, $scope.controllerId)
                .catch(function (response) {
                    ToastService.ErrorFromResponse(response)
                })
                .finally(updateData)
        };

        $scope.resetChartDates = function () {
            $scope.chartDates = {
                startDate: '',
                endDate: ''
            }
        };

        $scope.formatDuration = function (duration) {
            var durStr = moment.duration(duration).format("d [day], h [hour], m [minute], s [second]");
            var durList = durStr.split(', ')
            var results = [];
            for (var i = 0; i < durList.length; i++) {
                if (durList[i].charAt(0) != '0') {
                    if (parseInt(durList[i].split(' ')[0]) > 1) {
                        results.push(durList[i] + 's')
                    } else {
                        results.push(durList[i])
                    }

                }
            }
            return results.join(', ')
        }

        $scope.$watch('coolerControls.state', function () {
            if (!$scope.coolerId || $scope.updating) {
                return
            }
            setOuterGaugeColor();
            ActuatorService.SetState($scope.coolerId, $scope.coolerControls.state);
        });

        $scope.$watch('coolerControls.override', function () {
            if (!$scope.coolerId || $scope.updating) {
                return
            }
            ActuatorService.SetOverride($scope.coolerId, $scope.coolerControls.override);
        });

        $scope.$watch('heaterControls.state', function () {
            if (!$scope.heaterId || $scope.updating) {
                return
            }
            setOuterGaugeColor();
            ActuatorService.SetState($scope.heaterId, $scope.heaterControls.state);
        });

        $scope.$watch('heaterControls.override', function () {
            if (!$scope.heaterId || $scope.updating) {
                return
            }
            ActuatorService.SetOverride($scope.heaterId, $scope.heaterControls.override);
        });

        $scope.$watch('chartDates', function (oldValue, newValue) {

            var startDate = moment($scope.chartDates.startDate);
            var endDate = moment();
            if ($scope.chartDates.endDate != '') {
                endDate = moment($scope.chartDates.endDate)
            }

            if (moment.duration(endDate.diff(startDate)).asHours() > 24 ||
                moment($scope.chartDates.startDate).isAfter($scope.chartDates.endDate)) {
                if (oldValue.startDate != newValue.startDate) {
                    $scope.chartDates.endDate = moment(startDate).add(24, 'hours').toDate();
                } else {
                    $scope.chartDates.startDate = moment(endDate).subtract(24, 'hours').toDate();
                }
            }

            updateChart($scope.coolerId, $scope.coolerChartOptions);
            updateChart($scope.brewData.id, $scope.brewChartOptions);
        }, true);

        $interval(function () {
            if ($scope.active && $scope.getRemainingControllerTime() <= 0 && !$scope.complete) {
                $scope.complete = true;
            }
            $scope.phaseTimeRemaining = $scope.formatDuration($scope.getRemainingPhaseTime());
            $scope.controllerTimeRemaining = $scope.formatDuration($scope.getRemainingControllerTime());
            $scope.remainingControllerPercent = $scope.getRemainingControllerPercent();
            $scope.remainingPhasePercent = $scope.getRemainingPhasePercent();

        }, 200);

        $scope.start = function () {
            // stops any running interval to avoid two intervals running at the same time
            $scope.stop();
            // store the interval promise
            $timeout(function () {
                updateData();
                $scope.updater = $interval(function () {
                    updateData();
                }, 15000)
            }, 0);
        };

        // stops the interval
        $scope.stop = function () {
            if ($scope.updater) {
                $interval.cancel($scope.updater);
            }
        };

        $scope.$on('$destroy', function () {
            $scope.stop();
        });

        $scope.start();
    }
})();