/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.brews', [
        'BlurAdmin.widgets'
    ])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('brews', {
                url: '/brews',
                template: '<ui-view></ui-view>',
                abstract: true,
                title: 'Brews',
                sidebarMeta: {
                    icon: 'ion-beer',
                    order: 200,
                },
            });
    }

})();
