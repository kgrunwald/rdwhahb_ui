/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.dashboard')
        .controller('BlurFeedCtrl', BlurFeedCtrl);

    /** @ngInject */
    function BlurFeedCtrl($scope, BrewService, $state) {

        $scope.feed = [];

        BrewService.GetAll().then(function (response) {
            response.data.forEach(function (brew) {
                var brewData = {
                    name: brew.name,
                    currentTemp: null,
                    setTemp: null,
                    statusColor: null,
                    id: brew.id
                }
                $scope.feed.push(brewData);
                brew.controllers.forEach(function (controller) {
                    if (controller.active) {

                        // Check if brew is at an acceptable temp
                        var color = 'success';
                        if (Math.abs(controller.sensor.value - controller.currentSetPoint) > controller.currentAcceptableDrift) {
                            color = 'warning';
                        }

                        brewData.name = brew.name
                        brewData.currentTemp = controller.sensor.value
                        brewData.setTemp = controller.currentSetPoint
                        brewData.statusColor = color
                        brewData.id = brew.id

                    }
                });
            });
        });

        $scope.goToBrew = function (brew) {
            $state.go('brews.brew' + brew.id)
        }

    }
})();