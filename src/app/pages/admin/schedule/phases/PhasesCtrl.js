/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.admin')
        .controller('PhasesCtrl', PhasesCtrl);

    /** @ngInject */
    function PhasesCtrl($scope, baConfig, editableOptions, ToastService) {

        editableOptions.theme = 'bs3';
        $scope.phasesBackup = null;
        $scope.editing = {};
        $scope.math = Math;

        $scope.transparent = baConfig.theme.blur;
        var dashboardColors = baConfig.colors.dashboard;
        var colors = [];
        for (var key in dashboardColors) {
            colors.push(dashboardColors[key]);
        }

        function getRandomColor() {
            var i = Math.floor(Math.random() * (colors.length - 1));
            return colors[i];
        }

        $scope.showEditing = function (index) {
            $scope.editing[index] = true
        }

        $scope.hideEditing = function (index) {
            $scope.editing[index] = false
        }

        $scope.isEditing = function () {
            for (var key in $scope.editing) {
                if ($scope.editing[key]) {
                    return true
                }
            }
            return false
        }

        $scope.addPhase = function () {
            if (!$scope.formData.durationDays) {
                $scope.formData.durationDays = 0
            }
            if (!$scope.formData.durationHours) {
                $scope.formData.durationHours = 0
            }
            if (!$scope.formData.durationMinutes) {
                $scope.formData.durationMinutes = 0
            }
            $scope.formData.duration = ($scope.formData.durationDays * 60 * 24) + ($scope.formData.durationHours * 60) + $scope.formData.durationMinutes
            if (!$scope.formData.duration) {
                ToastService.Open('error', 'Must have duration', 'Phase duration cannot be 0')
                return
            }
            $scope.formData.color = getRandomColor();
            $scope.ngModel.push($scope.formData);
            $scope.formData = {};
        };

        $scope.resetPhases = function () {
            $scope.ngModel = $scope.phasesBackup;
        };

        $scope.savePhases = function () {
            $scope.phasesBackup = null;
            $scope.updatePhase();
        }

        $scope.ngModel.forEach(function (item) {
            item.color = getRandomColor();
        });

        $scope.removePhase = function (index) {
            $scope.ngModel.splice(index, 1);
        }

        $scope.$watch('ngModel', function (newValue, oldValue) {
            if (JSON.stringify(newValue) != JSON.stringify(oldValue)) {
                if (JSON.stringify($scope.ngModel) != JSON.stringify($scope.phasesBackup)) {
                    if (!$scope.phasesBackup) {
                        $scope.phasesBackup = oldValue
                    }
                } else {
                    $scope.phasesBackup = null;
                }
            }
        }, true)
    }
})();