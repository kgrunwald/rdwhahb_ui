/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.admin')
        .directive('phases', phases)

    /** @ngInject */
    function phases() {
        return {
            restrict: 'EA',
            controller: 'PhasesCtrl',
            templateUrl: 'app/pages/admin/schedule/phases/phases.html',
            scope: {
                ngModel: '=',
                updatePhase: '&',
                phasesChanged: '='
            }
        };
    }

})();