/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.admin')
        .directive('schedule', schedule);

    /** @ngInject */
    function schedule() {
        return {
            restrict: 'EA',
            scope: {
                scheduleData: "=",
                scheduleTypes: "=",
                updateScheduleData: "&",
                removeScheduleData: "&",
            },
            templateUrl: 'app/pages/admin/schedule/schedule.html',
            controller: 'ScheduleCtrl'
        };
    }
})();