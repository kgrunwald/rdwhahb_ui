/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.admin')
        .controller('ScheduleCtrl', ScheduleCtrl);

    /** @ngInject */
    function ScheduleCtrl($scope, editableOptions) {

        editableOptions.theme = 'bs3';

        $scope.removeSchedule = function () {
            $scope.removeScheduleData($scope.scheduleData)
        };

        $scope.updateSchedule = function () {
            $scope.updateScheduleData($scope.scheduleData)
        };
    }
})();