/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.admin')
        .controller('BrewConfigCtrl', BrewConfigCtrl);

    /** @ngInject */
    function BrewConfigCtrl($scope, editableOptions) {

        editableOptions.theme = 'bs3';

        $scope.removeBrew = function () {
            $scope.removeBrewData($scope.brewData)
        };

        $scope.updateBrew = function () {
            $scope.updateBrewData($scope.brewData)
        };

        $scope.removeBrewController = function (controller) {
            $scope.removeControllerData({brew: $scope.brewData, controller: controller})
        };

        $scope.updateBrewController = function (controller) {
            $scope.updateControllerData({brew: $scope.brewData, controller: controller})

        };

        $scope.addBrewController = function (controller) {
            $scope.addControllerData({brew: $scope.brewData, controller: controller})

        };
    }
})();