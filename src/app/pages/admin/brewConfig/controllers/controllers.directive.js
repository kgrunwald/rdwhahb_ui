/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.admin')
        .directive('controllers', controllers)

    /** @ngInject */
    function controllers() {
        return {
            restrict: 'EA',
            controller: 'ControllersCtrl',
            templateUrl: 'app/pages/admin/brewConfig/controllers/controllers.html',
            scope: {
                ngModel: '=',
                updateItem: '&',
                addItem: '&',
                removeItem: '&',
                itemsChanged: '=',
                schedules: "=",
                sensors: "=",
                actuators: "=",
                controllerTypes: "="
            }
        };
    }

})();