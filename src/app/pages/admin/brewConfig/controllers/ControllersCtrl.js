/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.admin')
        .controller('ControllersCtrl', ControllersCtrl);

    /** @ngInject */
    function ControllersCtrl($scope, baConfig, editableOptions, $timeout) {

        editableOptions.theme = 'bs3';
        $scope.itemsBackup = null;
        $scope.editing = {}
        $scope.heaterActuators = []

        $scope.transparent = baConfig.theme.blur;
        var dashboardColors = baConfig.colors.dashboard;
        var colors = [];
        for (var key in dashboardColors) {
            colors.push(dashboardColors[key]);
        }

        function getRandomColor() {
            var i = Math.floor(Math.random() * (colors.length - 1));
            return colors[i];
        }

        $scope.showEditing = function (index) {
            $scope.editing[index] = true
        }

        $scope.hideEditing = function (index) {
            $scope.editing[index] = false
        }

        $scope.isEditing = function () {
            for (var key in $scope.editing) {
                if ($scope.editing[key]) {
                    return true
                }
            }
            return false
        }

        $scope.addItemToList = function () {
            $scope.formData.color = getRandomColor();
            $scope.ngModel.push($scope.formData);
            $scope.formData = {};
        };

        $scope.resetItems = function () {
            $scope.ngModel = $scope.itemsBackup;
        };

        $scope.saveItems = function () {

            for (var i = 0; i < $scope.ngModel.length; i++) {
                var found = false;
                for (var k = 0; k < $scope.itemsBackup.length; k++) {
                    if ($scope.ngModel[i].id == $scope.itemsBackup[k].id) {
                        $scope.itemsBackup[k].found = true;
                        found = true;
                        break;
                    }
                }
                if (found) {
                    $scope.updateItem({controller: $scope.ngModel[i]})
                } else {
                    $scope.addItem({controller: $scope.ngModel[i]})
                }
            }

            for (var k = 0; k < $scope.itemsBackup.length; k++) {
                if (!$scope.itemsBackup[k].found) {
                    $scope.removeItem({controller: $scope.itemsBackup[k]})
                }
            }

            $scope.itemsBackup = null;
        }

        $scope.ngModel.forEach(function (item) {
            item.color = getRandomColor();
        });

        $scope.removeItemFromList = function (index) {
            $scope.ngModel.splice(index, 1);
        }

        $scope.showItem = function (item, itemList) {
            var selected;
            if (item && itemList.length) {
                for (var i = 0; i < itemList.length; i++) {
                    if (item.id == itemList[i].id) {
                        selected = itemList[i].name
                    }
                }
                return selected ? selected : 'Not set';
            } else {
                return item.name || 'Not set';
            }
        };

        $scope.getHeaterActuators = function () {
            var hactuators = []
            for (var i = 0; i < $scope.actuators.length; i++) {
                if ($scope.actuators[i].role == 'HEATER') {
                    hactuators.push($scope.actuators[i])
                }
            }
            $scope.heaterActuators = hactuators;
        }

        $scope.$watch('actuators', function () {
            $scope.getHeaterActuators();
        })

        $scope.$watch('ngModel', function (newValue, oldValue) {
            if (JSON.stringify(newValue) != JSON.stringify(oldValue)) {
                if (JSON.stringify($scope.ngModel) != JSON.stringify($scope.itemsBackup)) {
                    if (!$scope.itemsBackup) {
                        $scope.itemsBackup = oldValue
                    }
                } else {
                    $scope.itemsBackup = null;
                }
            }
        }, true)

        $timeout($scope.getHeaterActuators, 0);

    }
})();