/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.admin')
        .directive('brewConfig', brewConfig);

    /** @ngInject */
    function brewConfig() {
        return {
            restrict: 'EA',
            scope: {
                brewData: "=",
                updateBrewData: "&",
                removeBrewData: "&",
                addControllerData: '&',
                removeControllerData: '&',
                updateControllerData: '&',
                carbonationMethods: "=",
                controllerTypes: "=",
                schedules: "=",
                sensors: "=",
                actuators: "="
            },
            templateUrl: 'app/pages/admin/brewConfig/brewConfig.html',
            controller: 'BrewConfigCtrl'
        };
    }
})();