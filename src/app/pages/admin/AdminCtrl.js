/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.admin')
        .controller('AdminCtrl', AdminCtrl);

    /** @ngInject */
    function AdminCtrl($scope, $timeout, $q, UserService, SensorService, ActuatorService,
                       ScheduleService, BrewService, CoolerService, ToastService) {

        $scope.scheduleFormData = {};
        $scope.coolerFormData = {};
        $scope.userFormData = {};
        $scope.actuatorFormData = {};
        $scope.brewFormData = {};
        $scope.errorMessage = null;

        $scope.roles = [];
        $scope.users = [];
        $scope.sensors = [];
        $scope.actuators = [];
        $scope.schedules = [];
        $scope.coolerActuators = [];

        $scope.actuatorTypes = [
            'GPIO',
            'M-POWER'
        ];

        $scope.actuatorRoles = [
            'COOLER',
            'HEATER'
        ];

        $scope.carbonationMethods = [
            'BOTTLE',
            'KEG'
        ];

        $scope.scheduleTypes = [
            'FERMENTATION',
            'LAGER'
        ];

        $scope.controllerTypes = [
            'FERMENTATION',
            'LAGER'
        ];

        $scope.closeErrorMessage = function () {
            $scope.errorMessage = null;
        }

        $scope.errorFromResponse = function (response) {
            var message = ""
            if (response.res.data) {
                if (response.res.data.message instanceof Object) {
                    var keys = Object.keys(response.res.data.message);
                    for (var i in keys) {
                        message = message + response.res.data.message[keys[i]] + ', '
                    }
                    message = message.slice(0, -2)
                } else {
                    message = message + response.res.data.message
                }
            } else {
                message = 'Unknown Error'
            }
            ToastService.Open('error', response.message, message)
        }

        $scope.updateUser = function (data, user) {
            data.id = user.id
            var roles = []
            for (var i = 0; i < data.roles.length; i++) {
                roles.push(data.roles[i].name);
            }
            if (!data.password) {
                delete data.password
            }
            UserService.Update(data)
                .catch(function (response) {
                    $scope.errorFromResponse(response)
                    $scope.updateTables();
                });
        };

        $scope.addUser = function () {
            UserService.Create($scope.userFormData)
                .then(function (response) {
                    $scope.userFormData = {};
                    $scope.users.push(response.data)
                })
                .catch(function (response) {
                    $scope.errorFromResponse(response)
                    $scope.updateTables();
                })
        };

        $scope.removeUser = function (user) {
            UserService.Delete(user.id)
                .then(function () {
                    for (var i = 0; i < $scope.users.length; i++) {
                        if ($scope.users[i].id == user.id) {
                            $scope.users.splice(i, 1);
                            break;
                        }
                    }
                })
                .catch(function (response) {
                    $scope.errorFromResponse(response)
                    $scope.updateTables();
                });
        };

        $scope.validateEmail = function (email) {

        };

        $scope.updateSensor = function (data, sensor) {
            data.id = sensor.id
            SensorService.Update(data)
                .then(function () {
                    if (data.name.toLowerCase().indexOf('test') > -1) {
                        SensorService.SetValue(sensor.id, data)
                            .catch(function (response) {
                                $scope.errorFromResponse(response)
                                $scope.updateTables();
                            })
                    }
                })
                .catch(function (response) {
                    $scope.errorFromResponse(response)
                    $scope.updateTables();
                });
        };

        $scope.updateActuator = function (data, actuator) {
            (data.override) ? data.override = true : data.override = false;
            (data.state) ? data.state = true : data.state = false;
            data.id = actuator.id;
            ActuatorService.Update(data)
                .catch(function (response) {
                    $scope.errorFromResponse(response);
                    $scope.updateTables()
                });
        };

        $scope.addActuator = function () {
            ActuatorService.Create($scope.actuatorFormData)
                .then(function (response) {
                    $scope.actuatorFormData = {};
                    $scope.actuators.push(response.data)
                })
                .catch(function (response) {
                    $scope.errorFromResponse(response)
                    $scope.updateTables()
                })
        };

        $scope.removeActuator = function (actuator) {
            ActuatorService.Delete(actuator.id)
                .then(function () {
                    for (var i = 0; i < $scope.actuators.length; i++) {
                        if ($scope.actuators[i].id == actuator.id) {
                            $scope.actuators.splice(i, 1);
                            break;
                        }
                    }
                })
                .catch(function (response) {
                    $scope.errorFromResponse(response)
                    $scope.updateTables()
                });
        };

        $scope.removeSchedule = function (schedule) {
            ScheduleService.Delete(schedule.id)
                .then(function () {
                    for (var i = 0; i < $scope.schedules.length; i++) {
                        if ($scope.schedules[i].id == schedule.id) {
                            $scope.schedules.splice(i, 1);
                            break;
                        }
                    }
                })
                .catch(function (response) {
                    $scope.errorFromResponse(response)
                    $scope.updateTables()
                });
        };

        $scope.updateSchedule = function (schedule) {
            ScheduleService.Update(schedule)
                .catch(function (response) {
                    $scope.errorFromResponse(response)
                    $scope.updateTables();
                });
        };

        $scope.addSchedule = function () {
            $scope.scheduleFormData.phases = []
            ScheduleService.Create($scope.scheduleFormData)
                .then(function (response) {
                    $scope.scheduleFormData = {};
                    $scope.schedules.push(response.data)
                })
                .catch(function (response) {
                    $scope.errorFromResponse(response)
                })
                .finally($scope.updateTables)
        };

        $scope.addBrewToCooler = function (coolerId, brewId) {
            CoolerService.AddBrew(coolerId, brewId)
                .catch(function () {
                    $scope.errorFromResponse(response)
                    $scope.updateTables()
                })
        }

        $scope.addBrew = function () {
            $scope.brewFormData.brewDate = null;
            BrewService.Create($scope.brewFormData)
                .then(function (response) {
                    var brewId = response.data.id
                    $scope.addBrewToCooler($scope.brewFormData.cooler, brewId);
                    $scope.brews.push(response.data);
                    $scope.brewFormData = {};
                })
                .catch(function (response) {
                    $scope.errorFromResponse(response)
                    $scope.updateTables()
                })

        };

        $scope.removeBrew = function (brew) {
            BrewService.Delete(brew.id)
                .then(function () {
                    for (var i = 0; i < $scope.brews.length; i++) {
                        if ($scope.brews[i].id == brew.id) {
                            $scope.brews.splice(i, 1);
                            break;
                        }
                    }
                })
                .catch(function (response) {
                    $scope.errorFromResponse(response)
                    $scope.updateTables()
                });
        };

        $scope.updateBrew = function (brew) {
            BrewService.Update(brew)
                .catch(function (response) {
                    $scope.errorFromResponse(response)
                    $scope.updateTables()
                })
        };

        $scope.addController = function (brew, controller) {
            var data = {}
            for (var key in controller) {
                if (controller[key] && controller[key].id) {
                    data[key + 'Id'] = controller[key].id
                }
            }
            data.type = controller.type;
            BrewService.AddController(brew.id, data)
                .then(function () {
                    $scope.brewFormData = {};
                })
                .catch(function (response) {
                    $scope.errorFromResponse(response)
                    $scope.updateTables()
                });
        };

        $scope.removeController = function (brew, controller) {
            BrewService.RemoveController(brew.id, controller.id)
                .catch(function (response) {
                    $scope.errorFromResponse(response)
                    $scope.updateTables()
                });
        };

        $scope.updateController = function (brew, controller) {
            var data = {}
            for (var key in controller) {
                if (controller[key] && controller[key].id) {
                    data[key + 'Id'] = controller[key].id
                }
            }
            data.type = controller.type;
            data.id = controller.id;
            data.active = controller.active ? true : false;
            BrewService.UpdateController(brew.id, data)
                .catch(function (response) {
                    $scope.errorFromResponse(response)
                    $scope.updateTables()
                });
        };

        $scope.updateCooler = function (data, cooler) {
            data.id = cooler.id
            CoolerService.Update(data)
                .catch(function (response) {
                    $scope.errorFromResponse(response)
                    $scope.updateTables()
                });
        };

        $scope.addCooler = function () {
            CoolerService.Create($scope.coolerFormData)
                .then(function (response) {
                    $scope.coolerFormData = {};
                    $scope.coolers.push(response.data)
                })
                .catch(function (response) {
                    $scope.errorFromResponse(response)
                    $scope.updateTables()
                })
        };

        $scope.removeCooler = function (cooler) {
            CoolerService.Delete(cooler.id)
                .then(function () {
                    for (var i = 0; i < $scope.coolers.length; i++) {
                        if ($scope.coolers[i].id == cooler.id) {
                            $scope.coolers.splice(i, 1);
                            break;
                        }
                    }
                })
                .catch(function (response) {
                    $scope.errorFromResponse(response)
                    $scope.updateTables()
                });
        };

        $scope.showRoles = function (user) {
            var selected = []

            var userRoles = new Array();

            for (var key in user.roles) {
                userRoles.push(user.roles[key].name);
            }

            if (user.roles && $scope.roles.length) {
                angular.forEach($scope.roles, function (r) {
                    if (userRoles.indexOf(r.name) >= 0) {
                        selected.push(r.name);
                    }
                });

                return selected.length ? selected.join(', ') : '';
            } else return 'Not set'
        };

        $scope.getCoolerActuators = function () {
            var hactuators = [];
            for (var i = 0; i < $scope.actuators.length; i++) {
                if ($scope.actuators[i].role == 'COOLER') {
                    hactuators.push($scope.actuators[i])
                }
            }
            return hactuators;
        };

        $scope.showItem = function (item, itemList) {
            var selected;
            if (item && itemList.length) {
                for (var i = 0; i < itemList.length; i++) {
                    if (item.id == itemList[i].id) {
                        selected = itemList[i].name
                    }
                }
                return selected ? selected : 'Not set';
            } else {
                return item.name || 'Not set';
            }
        };

        $scope.updateTables = function () {

            $q.props({
                //roles: UserService.GetAllRoles(),
                users: UserService.GetAll(),
                sensors: SensorService.GetAll(),
                actuators: ActuatorService.GetAll(),
                schedules: ScheduleService.GetAll(),
                brews: BrewService.GetAll(),
                coolers: CoolerService.GetAll(),
            }).then(function (results) {

                //$scope.roles = results.roles.data;
                $scope.users = results.users.data;
                $scope.sensors = results.sensors.data;
                $scope.actuators = results.actuators.data;
                $scope.schedules = results.schedules.data;
                $scope.brews = results.brews.data;
                $scope.coolers = results.coolers.data;
                $scope.coolerActuators = $scope.getCoolerActuators();
            });

        }

        $scope.$watch('errorMessage', function () {
            if ($scope.errorMessage) {
                $timeout(function () {
                    $scope.errorMessage = null;
                }, 5000);
            }
        });


        $timeout(function () {
            $scope.updateTables();
            //$scope.formData = {};
        }, 0);


    }
})();