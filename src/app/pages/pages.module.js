/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages', [
        'ui.router',
        'BlurAdmin.pages.dashboard',
        'BlurAdmin.pages.brews',
        'BlurAdmin.pages.logout',
        'BlurAdmin.pages.profile',
        'BlurAdmin.pages.admin',
        'BlurAdmin.pages.notifications'
    ])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($urlRouterProvider) {
        $urlRouterProvider.otherwise('/dashboard');
    }


})();
