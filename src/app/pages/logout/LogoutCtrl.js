/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.logout')
        .controller('LogoutCtrl', LogoutCtrl);

    /** @ngInject */
    function LogoutCtrl($state, AuthenticationService) {
        (function logout() {
            AuthenticationService.ClearCredentials();
        })();
    }
})();