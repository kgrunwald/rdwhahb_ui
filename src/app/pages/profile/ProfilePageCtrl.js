/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.profile')
        .controller('ProfilePageCtrl', ProfilePageCtrl);

    /** @ngInject */
    function ProfilePageCtrl($scope, $rootScope, UserService, ToastService) {
        $scope.user = $rootScope.globals.currentUser;

        $scope.submitUserForm = function (user) {

            var data = {};

            if (user.password) {
                if (user.password != user.confirmPassword) {
                    ToastService.Open('error', 'Passwords do not match!', 'Please make sure the passwords match')
                    return
                } else {
                    data.password = user.password
                }
            }

            data.id = user.id;
            user.profile.firstName ? data.firstName = user.profile.firstName : {};
            user.profile.lastName ? data.lastName = user.profile.lastName : {};
            user.email ? data.email = user.email : {};

            UserService.Update(data)
                .then(function (response) {
                    response.data.accessToken = $rootScope.globals.currentUser.accessToken;
                    $rootScope.globals.currentUser = response.data;
                    ToastService.Open('success', 'User Profile Updated Successfully')
                })
                .catch(function (response) {
                    ToastService.ErrorFromResponse(response)
                })

        }

    }

})();
