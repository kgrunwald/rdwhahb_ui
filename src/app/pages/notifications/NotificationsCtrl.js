/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages')
        .controller('NotificationsCtrl', NotificationsCtrl);

    /** @ngInject */
    function NotificationsCtrl($scope, $timeout, $interval, $stateParams, AlertService) {

        $scope.viewChoices = []
        $scope.viewOptions = [
            'ALL',
            'READ',
            'ACKNOWLEDGED',
            'RESOLVED',
            'NEW'
        ];

        $scope.notificationId = $stateParams.notificationId

        $scope.updateNotifications = function () {
            AlertService.GetAll().then(function (response) {
                $scope.feed = response.data
            });
        };


        $timeout(function () {
                $scope.updateNotifications()
                $interval($scope.updateNotifications, 15000)
            }, 0
        );

    }
})();