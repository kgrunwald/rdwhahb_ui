/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.notifications', [])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('notifications', {
                url: '/notifications',
                params: {
                    notificationId: null,
                },
                templateUrl: 'app/pages/notifications/notifications.html',
                title: 'Notifications',
                controller: 'NotificationsCtrl',
                scope: {
                    viewChoices: '='
                }
            });
    }

})();
