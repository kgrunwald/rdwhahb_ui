/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.notifications')
        .directive('notification', notification);

    /** @ngInject */
    function notification() {
        return {
            restrict: 'AE',
            controller: 'NotificationCtrl',
            templateUrl: 'app/pages/notifications/notification/notification.html',
            scope: {
                viewChoices: '=',
                notificationId: '='
            }
        };
    }
})();