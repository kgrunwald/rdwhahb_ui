/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.notifications')
        .controller('NotificationCtrl', NotificationCtrl);

    /** @ngInject */
    function NotificationCtrl($scope, $timeout, $location, $interval, $anchorScroll, AlertService, ToastService) {

        $scope.feed = []
        $scope.notifications = []

        $scope.hgt = window.innerHeight - 255

        $scope.updateNotifications = function () {
            AlertService.GetAll().then(function (response) {

                var newNotifications = response.data

                for (var i = 0; i < newNotifications.length; i++) {
                    var found = false;
                    if (!$scope.notifications.length) {
                        found = false;
                    } else {
                        for (var k = 0; k < $scope.notifications.length; k++) {
                            if ($scope.notifications[k].id == newNotifications[i].id) {
                                if ($scope.notifications[k].alertState != newNotifications[i].alertState) {
                                    $scope.notifications[k] = newNotifications[i];
                                }
                                found = true;
                                break;
                            }
                        }
                    }
                    if (!found) {
                        $scope.notifications.push(newNotifications[i])
                    }
                }

                for (var k = $scope.notifications.length - 1; k >= 0; k--) {
                    var found = false;
                    for (var i = 0; i < newNotifications.length; i++) {
                        if ($scope.notifications[k].id == newNotifications[i].id) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        $scope.notifications.splice(k, 1);
                    }
                }

                $scope.filterNotifications();
                if ($scope.notificationId) {
                    $scope.goToAnchor($scope.notificationId);
                    $scope.notificationId = null;
                }

            });
        };

        $scope.goToAnchor = function (x) {
            var newHash = 'anchor' + x;
            if ($location.hash() !== newHash) {
                $location.hash('anchor' + x);
            } else {
                $anchorScroll();
            }
        };

        $scope.$watch('viewChoices', function () {
            $scope.filterNotifications();
        })

        $scope.filterNotifications = function () {
            //var tmpNotifications = []
            for (var i = 0; i < $scope.notifications.length; i++) {
                $scope.notifications[i].shown = false;
                if (!$scope.viewChoices.length) {
                    if ($scope.notifications[i].read == false) {
                        $scope.notifications[i].shown = true;
                    }
                    continue;
                }
                if ($scope.notifications[i].read == false) {
                    if (!($scope.viewChoices.indexOf('ALL') > -1)) {
                        if ($scope.viewChoices.indexOf($scope.notifications[i].alertState) > -1 || !$scope.viewChoices.length) {
                            $scope.notifications[i].shown = true;
                        }
                    } else {
                        if (!($scope.viewChoices.indexOf($scope.notifications[i].alertState) > -1)) {
                            $scope.notifications[i].shown = true;
                        }
                    }
                } else {
                    if (!($scope.viewChoices.indexOf('ALL') > -1)) {
                        if ($scope.viewChoices.indexOf('READ') > -1 || !$scope.viewChoices.length) {
                            $scope.notifications[i].shown = true;
                        }
                    } else {
                        if (!($scope.viewChoices.indexOf('READ') > -1)) {
                            $scope.notifications[i].shown = true;
                        }
                    }
                }
            }
        }

        $scope.acknowledgeAlert = function (notification) {
            AlertService.Acknowledge(notification.id)
                .then(function (response) {
                    for (var i = 0; i < $scope.notifications.length; i++) {
                        if ($scope.notifications[i].id == notification.id) {
                            $scope.notifications[i] = response.data
                            $scope.filterNotifications();
                            break;
                        }
                    }
                })
                .catch(function (response) {
                    ToastService.ErrorFromResponse(response)
                })
        }

        $scope.readAlert = function (notification) {
            AlertService.Read(notification.id, !notification.read)
                .then(function (response) {
                    for (var i = 0; i < $scope.notifications.length; i++) {
                        if ($scope.notifications[i].id == notification.id) {
                            $scope.notifications[i].read = response.data.read
                            $scope.filterNotifications();
                            break;
                        }
                    }
                })
                .catch(function (response) {
                    ToastService.ErrorFromResponse(response)
                })
        }

        $scope.getRelativeTime = function (msg) {
            var notifyTime = moment(msg.timestamp)
            var timeDiff = moment.duration(moment().diff(notifyTime))
            var count = 0;
            var diffStr = "";
            if (timeDiff.days()) {
                count = timeDiff.days()
                diffStr = timeDiff.days().toString() + ' day'
            } else if (timeDiff.hours()) {
                count = timeDiff.hours()
                diffStr = timeDiff.hours().toString() + ' hour'
            } else if (timeDiff.minutes()) {
                count = timeDiff.minutes()
                diffStr = timeDiff.minutes().toString() + ' minute'
            } else {
                count = timeDiff.seconds()
                diffStr = timeDiff.seconds().toString() + ' second'
            }
            if (count == 1) {
                diffStr = diffStr + ' ago'
            } else {
                diffStr = diffStr + 's ago'
            }
            return diffStr
        }

        $scope.expandNotification = function (notification) {
            notification.expanded = !notification.expanded;
        }

        $scope.start = function () {
            // stops any running interval to avoid two intervals running at the same time
            $scope.stop();
            // store the interval promise
            $timeout(function () {
                $scope.updateNotifications();
                $scope.updater = $interval(function () {
                    $scope.updateNotifications();
                }, 5000)
            }, 1000);
        };

        // stops the interval
        $scope.stop = function () {
            if ($scope.updater) {
                $interval.cancel($scope.updater);
            }
        };

        $scope.$on('$destroy', function () {
            $scope.stop();
        });

        $scope.start();

    }
})();