/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.theme.components')
        .controller('MsgCenterCtrl', MsgCenterCtrl);

    /** @ngInject */
    function MsgCenterCtrl($scope, $sce, $timeout, $interval, AlertService, $state) {

        $scope.alerts = [];
        $scope.notifications = [];

        $scope.updateNotifications = function () {
            AlertService.GetAll()
                .then(function (response) {
                    $scope.notifications = response.data;

                    for (var i = 0; i < $scope.notifications.length; i++) {
                        var found = false;
                        if ($scope.notifications[i].alertState == "RESOLVED") {
                            continue;
                        }
                        if (!$scope.alerts.length) {
                            found = false;
                        } else {
                            for (var k = 0; k < $scope.alerts.length; k++) {

                                if ($scope.alerts[k].id == $scope.notifications[i].id) {
                                    if ($scope.alerts[k].alertState != $scope.notifications[i].alertState) {
                                        $scope.alerts[k].alertState = $scope.notifications[i].alertState;
                                    }
                                    found = true;
                                    break;
                                }
                            }
                        }
                        if (!found) {
                            $scope.alerts.push($scope.notifications[i])
                        }
                    }


                    for (var k = $scope.alerts.length - 1; k >= 0; k--) {
                        var found = false;
                        for (var i = 0; i < $scope.notifications.length; i++) {
                            if ($scope.alerts[k].id == $scope.notifications[i].id && $scope.notifications[i].alertState != 'RESOLVED') {
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            $scope.alerts.splice(k, 1);
                        }
                    }
                })
                .catch(function (response) {
                })
        }

        $scope.getAlerts = function (item) {
            if (item.alertState != 'RESOLVED') {
                return true
            } else {
                return false
            }

        };

        $scope.getMessage = function (msg) {
            var text = msg.name;
            return $sce.trustAsHtml(text);
        };

        $scope.goToNotification = function (notification) {
            $state.go('notifications', {notificationId: notification.id})
        }

        $scope.getRelativeTime = function (msg) {
            var notifyTime = moment(msg.timestamp)
            var timeDiff = moment.duration(moment().diff(notifyTime))
            var count = 0;
            var diffStr = "";
            if (timeDiff.days()) {
                count = timeDiff.days()
                diffStr = timeDiff.days().toString() + ' day'
            } else if (timeDiff.hours()) {
                count = timeDiff.hours()
                diffStr = timeDiff.hours().toString() + ' hour'
            } else if (timeDiff.minutes()) {
                count = timeDiff.minutes()
                diffStr = timeDiff.minutes().toString() + ' minute'
            } else {
                count = timeDiff.seconds()
                diffStr = timeDiff.seconds().toString() + ' second'
            }
            if (count == 1) {
                diffStr = diffStr + ' ago'
            } else {
                diffStr = diffStr + 's ago'
            }
            return diffStr
        }

        $timeout(function () {
            $scope.updateNotifications()
            $interval($scope.updateNotifications, 15000)
        }, 0)

    }
})();