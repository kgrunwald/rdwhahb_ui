'use strict';

angular.module('BlurAdmin', [
    'ngAnimate',
    'ui.bootstrap',
    'ui.sortable',
    'ui.router',
    'ui.bootstrap.datetimepicker',
    'ngTouch',
    'toastr',
    'smart-table',
    'xeditable',
    'checklist-model',
    'ui.slimscroll',
    'ngJsTree',
    'angular-progress-button-styles',
    'BlurAdmin.theme',
    'BlurAdmin.pages',
    'BlurAdmin.widgets',
    'ui.knob',
    'ngRoute',
    'ngCookies',
    'angular-jwt',
    'mwl.bluebird',
    'timer'
])
    .constant('baseUrl', 'https://www.jkbrewery.com')
    .provider('$brewsState', provider)
    .run(run)
    .config(['$logProvider', function ($logProvider) {
        $logProvider.debugEnabled(true);
    }]);

provider.$inject = ['$stateProvider'];
function provider($stateProvider) {
    this.$get = function () {
        return {
            addState: function (id, name) {
                $stateProvider
                    .state('brews.brew' + id, {
                        id: id,
                        restrict: 'AE',
                        url: '/brew/' + id,
                        templateUrl: 'app/pages/brews/brew/brew.html',
                        title: name,
                        controller: 'BrewCtrl',
                        scope: {
                            coolColor: "=",
                            innerData: "=",
                            coolerId: "=",
                            brewData: "=",
                            currentAcceptableDrift: "=",
                            coolerControls: "=",
                            heaterControls: "=",
                            heaterId: "=",
                            heatColor: "=",
                            tempColor: "=",
                            brewTempData: "=",
                            brewTempLabels: "=",
                            coolerChartOptions: "=",
                            brewChartOptions: "=",
                            brewId: '=',
                            controllerId: '=',
                            zoom: '=',
                            chartDates: '=',
                            controllers: '=',
                            phaseIndex: '='
                        },
                        sidebarMeta: {
                            order: 1000,
                        },
                    });
            },
        }
    }
}

run.$inject = ['$rootScope', '$cookieStore', '$http', '$brewsState', '$state', 'AuthenticationService', 'modalService', 'baseUrl'];
function run($rootScope, $cookieStore, $http, $brewsState, $state, AuthenticationService, modalService, baseUrl) {

    $rootScope.shadeBlend = function (p, c0, c1) {
        var n = p < 0 ? p * -1 : p, u = Math.round, w = parseInt;
        if (c0.length > 7) {
            var f = c0.split(","), t = (c1 ? c1 : p < 0 ? "rgb(0,0,0)" : "rgb(255,255,255)").split(","), R = w(f[0].slice(4)), G = w(f[1]), B = w(f[2]);
            return "rgb(" + (u((w(t[0].slice(4)) - R) * n) + R) + "," + (u((w(t[1]) - G) * n) + G) + "," + (u((w(t[2]) - B) * n) + B) + ")"
        } else {
            var f = w(c0.slice(1), 16), t = w((c1 ? c1 : p < 0 ? "#000000" : "#FFFFFF").slice(1), 16), R1 = f >> 16, G1 = f >> 8 & 0x00FF, B1 = f & 0x0000FF;
            return "#" + (0x1000000 + (u(((t >> 16) - R1) * n) + R1) * 0x10000 + (u(((t >> 8 & 0x00FF) - G1) * n) + G1) * 0x100 + (u(((t & 0x0000FF) - B1) * n) + B1)).toString(16).slice(1)
        }
    }

    // Function to verify user roles
    $rootScope.verifyRoles = function (roles) {
        var returnVal = false;
        if ($rootScope.globals.currentUser) {
            $rootScope.globals.currentUser.roles.forEach(function (role) {
                if (roles.indexOf(role.name) > -1) {
                    returnVal = true;
                }
            });
        }
        return returnVal;
    };

    // Returns true of the user is logged in
    $rootScope.loggedIn = function () {
        if ($rootScope.globals.currentUser) {
            return true;
        }
        return false;
    };

    // Log user out
    $rootScope.logOut = function () {
        AuthenticationService.ClearCredentials();
        $state.go($state.current, {}, {reload: true});
    };

    // Send alert when the user is logged out
    $rootScope.alertLogOut = function () {
        $rootScope.logOut()
        var modalOptions = {
            headerText: 'Session Expired',
            bodyText: 'Please login again.',
            type: 'danger',
            ok: function () {
            }
        };

        var modalDefaults = {
            size: 'sm',
            backdrop: true,
            keyboard: true,
            modalFade: true,
            templateUrl: 'app/theme/components/modalTemplates/alertModal.html'
        };

        modalService.showModal(modalDefaults, modalOptions)
    };

    $rootScope.setBackgroundColor = function (color) {
        if (color) {
            $('.al-main').css('background-color', $rootScope.shadeBlend(.5, color))
        } else {
            $('.al-main').css('background-color', '')
        }
    }

    // redirect to login page if not logged in and trying to access a restricted page
    $rootScope.$on('$stateChangeStart',
        function (event, toState) {
            $rootScope.setBackgroundColor()
            var restrictedPage = $.inArray(toState.name, ['admin']) === 0;
            var loggedIn = $rootScope.loggedIn();
            if (restrictedPage && !loggedIn) {
                event.preventDefault();
                $state.transitionTo('dashboard');
            }
        });

    // Set default header
    $http.defaults.headers.common['Content-Type'] = 'application/json';

    // Create brew pages
    $http.get(baseUrl + '/brews').success(function (data) {
        var foundBrew = false
        data.forEach(function (brew) {
            $brewsState.addState(brew.id, brew.name);
            foundBrew = true
        });
        if (foundBrew) {
            $state.go('brews.brew' + data[0].id);
        } else {
            $state.go('dashboard');
        }
    });
    // keep user logged in after page refresh
    $rootScope.globals = $cookieStore.get('globals') || {};
    if ($rootScope.loggedIn()) {
        AuthenticationService.LoadCredentials();
    }
}
