(function () {
    'use strict';

    angular
        .module('BlurAdmin')
        .factory('UserService', UserService);

    UserService.$inject = ['baseUrl', '$http', '$rootScope'];
    function UserService(baseUrl, $http, $rootScope) {
        var service = {};

        service.Authenticate = Authenticate;
        service.GetAll = GetAll;
        service.GetByEmail = GetByEmail;
        service.GetById = GetById;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
        service.RefreshToken = RefreshToken;
        service.GetAllRoles = GetAllRoles;
        service.ValidateEmail = ValidateEmail;

        return service;

        function Authenticate(user) {
            return $http.post(baseUrl + '/auth', JSON.stringify(user))
                .then(handleSuccess, handleError('Invalid email and password'));
        }

        function ValidateEmail(email) {
            return $http.get(baseUrl + '/users/validate?email=' + email)
                .then(handleSuccess, handleError('Invalid email'));
        }

        function GetAll() {
            return $http.get(baseUrl + '/users')
                .then(handleSuccess, handleError('Error getting all users'));
        }

        function GetByEmail(email) {
            return $http.get(baseUrl + '/users?email=' + email)
                .then(handleSuccess, handleError('Error getting user by email'));
        }

        function GetById(id) {
            return $http.get(baseUrl + '/users/' + id)
                .then(handleSuccess, handleError('Error getting user by email'));
        }

        function Create(user) {
            return $http.post(baseUrl + '/users', user)
                .then(handleSuccess, handleError('Error creating user'));
        }

        function Update(user) {
            return $http.put(baseUrl + '/users/' + user.id, user)
                .then(handleSuccess, handleError('Error updating user'));
        }

        function Delete(id) {
            return $http.delete(baseUrl + '/users/' + id)
                .then(handleSuccess, handleError('Error deleting user'));
        }

        function RefreshToken(id) {
            return $http.get(baseUrl + '/users/' + id + '/token')
                .then(handleSuccess, handleError('Error deleting user'));
        }

        function GetAllRoles() {
            return $http.get(baseUrl + '/roles')
                .then(handleSuccess, handleError('Error getting all roles'));
        }

        // private functions

        function handleSuccess(res) {
            return {success: true, data: res.data};
        }

        function handleError(error) {
            return function (response) {
                throw {success: false, message: error, res: response}
            };
        }
    }

})();