(function () {
    'use strict';

    angular
        .module('BlurAdmin')
        .factory('ScheduleService', ScheduleService);

    ScheduleService.$inject = ['baseUrl', '$http', '$httpParamSerializer'];
    function ScheduleService(baseUrl, $http, $httpParamSerializer) {
        var service = {};

        service.GetAll = GetAll;
        service.GetById = GetById;
        service.Update = Update;
        service.Create = Create;
        service.Delete = Delete;

        return service;

        function GetAll(options) {
            var queryStr = $httpParamSerializer(options);
            return $http.get(baseUrl + '/schedules?' + queryStr)
                .then(handleSuccess, handleError('Error getting all schedules'));
        }

        function GetById(id) {
            return $http.get(baseUrl + '/schedules/' + id)
                .then(handleSuccess, handleError('Error getting schedule by id'));
        }

        function Update(schedule) {
            return $http.put(baseUrl + '/schedules/' + schedule.id, schedule)
                .then(handleSuccess, handleError('Error updating schedule'));
        }

        function Create(schedule) {
            return $http.post(baseUrl + '/schedules', schedule)
                .then(handleSuccess, handleError('Error creating schedule'));
        }

        function Delete(id) {
            return $http.delete(baseUrl + '/schedules/' + id)
                .then(handleSuccess, handleError('Error deleting schedule'));
        }

        // private functions

        function handleSuccess(res) {
            return {success: true, data: res.data};
        }

        function handleError(error) {
            return function (response) {
                throw {success: false, message: error, res: response}
            };
        }
    }

})();