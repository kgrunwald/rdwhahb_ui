(function () {
    'use strict';

    angular
        .module('BlurAdmin')
        .factory('CoolerService', CoolerService);

    CoolerService.$inject = ['baseUrl', '$http'];
    function CoolerService(baseUrl, $http) {
        var service = {};

        service.GetAll = GetAll;
        service.GetById = GetById;
        service.AddBrew = AddBrew;
        service.RemoveBrew = RemoveBrew;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;

        return service;

        function GetAll() {
            return $http.get(baseUrl + '/coolers')
                .then(handleSuccess, handleError('Error getting all coolers'));
        }

        function GetById(id) {
            return $http.get(baseUrl + '/coolers/' + id)
                .then(handleSuccess, handleError('Error getting cooler by id'));
        }

        function AddBrew(id, brewId) {
            var data = {
                brewId: brewId
            }
            return $http.post(baseUrl + '/coolers/' + id + '/brews', data)
                .then(handleSuccess, handleError('Error adding brew to cooler'));
        }

        function RemoveBrew(id, brew) {
            return $http.delete(baseUrl + '/coolers/' + id + '/brews/' + brew.id)
                .then(handleSuccess, handleError('Error removing brew from cooler'));
        }

        function Create(cooler) {
            return $http.post(baseUrl + '/coolers', cooler)
                .then(handleSuccess, handleError('Error creating cooler'));
        }

        function Update(cooler) {
            return $http.put(baseUrl + '/coolers/' + cooler.id, cooler)
                .then(handleSuccess, handleError('Error updating cooler'));
        }

        function Delete(id) {
            return $http.delete(baseUrl + '/coolers/' + id)
                .then(handleSuccess, handleError('Error deleting cooler'));
        }

        // private functions

        function handleSuccess(res) {
            return {success: true, data: res.data};
        }

        function handleError(error) {
            return function (response) {
                throw {success: false, message: error, res: response}
            };
        }
    }

})();