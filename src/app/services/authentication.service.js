(function () {
    'use strict';

    angular
        .module('BlurAdmin')
        .factory('AuthenticationService', AuthenticationService);

    AuthenticationService.$inject = ['$http', '$cookieStore', '$rootScope', '$timeout', 'UserService', 'jwtHelper', 'modalService'];
    function AuthenticationService($http, $cookieStore, $rootScope, $timeout, UserService, jwtHelper, modalService) {
        var service = {};

        service.Login = Login;
        service.LoggedIn = LoggedIn;
        service.SetCredentials = SetCredentials;
        service.ClearCredentials = ClearCredentials;
        service.LoadCredentials = LoadCredentials;
        service.RefreshCredentials = RefreshCredentials;

        return service;

        function Login(email, password, callback) {

            $timeout(function () {
                var user = {'email': email, 'password': password}
                UserService.Authenticate(user)
                    .then(function (response) {
                        callback(response);
                    })
                    .catch(function () {
                        callback();
                    });
            }, 1000);
        }

        function LoggedIn() {
            return $rootScope.loggedIn();
        }

        function SetCredentials(data) {
            ClearCredentials()
            $rootScope.globals = {
                currentUser: data
            };
            $http.defaults.headers.common['X-API-Key'] = 'b00cdc83-6bd0-45bb-a6ac-2f8ef357ecac';
            $http.defaults.headers.common['Authorization'] = 'Bearer ' + data.accessToken; // jshint ignore:line
            $cookieStore.put('globals', $rootScope.globals);
            var logoutTime = jwtHelper.getTokenExpirationDate($rootScope.globals.currentUser.accessToken) - (new Date());
            $rootScope.logoutTimer = $timeout(function () {
                var modalOptions = {
                    headerText: 'Session About to Expire',
                    bodyText: 'Would you like to remain logged in?',
                    type: 'warning',
                    autoClose: (1000 * 60 * 5) - 1000,
                    ok: function () {
                        RefreshCredentials();
                    },
                    cancel: function () {
                    },
                };

                var modalDefaults = {
                    backdrop: true,
                    keyboard: true,
                    modalFade: true,
                    templateUrl: 'app/theme/components/modalTemplates/alertModal.html'
                };

                modalService.showModal(modalDefaults, modalOptions)
                $rootScope.logoutTimer = $timeout(function () {
                    $rootScope.alertLogOut();
                }, (1000 * 60 * 5) - 1000);
            }, logoutTime - (1000 * 60 * 5));
        }

        function ClearCredentials() {
            $rootScope.globals = {};
            $cookieStore.remove('globals');
            $http.defaults.headers.common.Authorization = 'Bearer';
            if ($rootScope.logoutTimer) {
                $timeout.cancel($rootScope.logoutTimer)
                $rootScope.logoutTimer = null;
            }
            ;
        }

        function LoadCredentials() {
            $http.defaults.headers.common['X-API-Key'] = 'b00cdc83-6bd0-45bb-a6ac-2f8ef357ecac';
            try {
                var logoutTime = jwtHelper.getTokenExpirationDate($rootScope.globals.currentUser.accessToken) - (new Date())
                if (logoutTime < 0) {
                    $rootScope.logOut();
                }
                else {
                    SetCredentials($rootScope.globals.currentUser);
                    UserService.GetById($rootScope.globals.currentUser.id)
                        .then(function (response) {
                            if (response) {
                                var user = response.data;
                                user.accessToken = $rootScope.globals.currentUser.accessToken;
                                $rootScope.globals.currentUser = user;
                                $cookieStore.put('globals', $rootScope.globals);
                            }
                        })
                        .finally(function () {
                            $rootScope.loading = false;
                        });
                }
            }
            catch (err) {
                ClearCredentials();
            }
        }

        function RefreshCredentials() {
            UserService.RefreshToken($rootScope.globals.currentUser.id)
                .then(function (response) {
                    $rootScope.globals.currentUser.accessToken = response.data.accessToken;
                    SetCredentials($rootScope.globals.currentUser);
                });
        }

    }

    // Base64 encoding service used by AuthenticationService
    var Base64 = {

        keyStr: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',

        encode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            do {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);

                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;

                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }

                output = output +
                    this.keyStr.charAt(enc1) +
                    this.keyStr.charAt(enc2) +
                    this.keyStr.charAt(enc3) +
                    this.keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);

            return output;
        },

        decode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
            var base64test = /[^A-Za-z0-9\+\/\=]/g;
            if (base64test.exec(input)) {
                window.alert("There were invalid base64 characters in the input text.\n" +
                    "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                    "Expect errors in decoding.");
            }
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

            do {
                enc1 = this.keyStr.indexOf(input.charAt(i++));
                enc2 = this.keyStr.indexOf(input.charAt(i++));
                enc3 = this.keyStr.indexOf(input.charAt(i++));
                enc4 = this.keyStr.indexOf(input.charAt(i++));

                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;

                output = output + String.fromCharCode(chr1);

                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }

                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";

            } while (i < input.length);

            return output;
        }
    };

})();