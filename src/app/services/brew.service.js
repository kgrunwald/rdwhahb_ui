(function () {
    'use strict';

    angular
        .module('BlurAdmin')
        .factory('BrewService', BrewService);

    BrewService.$inject = ['baseUrl', '$http'];
    function BrewService(baseUrl, $http) {
        var service = {};

        service.GetAll = GetAll;
        service.GetById = GetById;
        service.AddController = AddController;
        service.RemoveController = RemoveController;
        service.UpdateController = UpdateController;
        service.StartController = StartController;
        service.StopController = StopController;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;

        return service;

        function GetAll() {
            return $http.get(baseUrl + '/brews')
                .then(handleSuccess, handleError('Error getting all brews'));
        }

        function GetById(id) {
            return $http.get(baseUrl + '/brews/' + id)
                .then(handleSuccess, handleError('Error getting brew by id'));
        }

        function AddController(brewId, controller) {
            return $http.post(baseUrl + '/brews/' + brewId + '/controllers', controller)
                .then(handleSuccess, handleError('Error adding controller to brew'));
        }

        function RemoveController(brewId, controllerId) {
            return $http.delete(baseUrl + '/brews/' + brewId + '/controllers/' + controllerId)
                .then(handleSuccess, handleError('Error deleting controller from brew'));
        }

        function UpdateController(brewId, controller) {
            return $http.put(baseUrl + '/brews/' + brewId + '/controllers/' + controller.id, controller)
                .then(handleSuccess, handleError('Error updating controller'));
        }

        function Create(brew) {
            return $http.post(baseUrl + '/brews', brew)
                .then(handleSuccess, handleError('Error creating brew'));
        }

        function Update(brew) {
            return $http.put(baseUrl + '/brews/' + brew.id, brew)
                .then(handleSuccess, handleError('Error updating brew'));
        }

        function Delete(id) {
            return $http.delete(baseUrl + '/brews/' + id)
                .then(handleSuccess, handleError('Error deleting brew'));
        }

        function StartController(brewId, controllerId) {
            var data = {active: true, forceRestart: true}
            return $http.put(baseUrl + '/brews/' + brewId + '/controllers/' + controllerId, data)
                .then(handleSuccess, handleError('Error starting brew'));
        }

        function StopController(brewId, controllerId) {
            var data = {active: false}
            return $http.put(baseUrl + '/brews/' + brewId + '/controllers/' + controllerId, data)
                .then(handleSuccess, handleError('Error stopping brew'));
        }

        // private functions

        function handleSuccess(res) {
            return {success: true, data: res.data};
        }

        function handleError(error) {
            return function (response) {
                throw {success: false, message: error, res: response}
            };
        }
    }

})();