(function () {
    'use strict';

    angular
        .module('BlurAdmin')
        .factory('ActuatorService', ActuatorService);

    ActuatorService.$inject = ['baseUrl', '$http', '$httpParamSerializer'];
    function ActuatorService(baseUrl, $http, $httpParamSerializer) {
        var service = {};

        service.GetAll = GetAll;
        service.GetById = GetById;
        service.GetHistory = GetHistory;
        service.SetState = SetState;
        service.SetOverride = SetOverride;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;

        return service;

        function GetAll(options) {
            var queryStr = $httpParamSerializer(options);
            return $http.get(baseUrl + '/actuators?' + queryStr)
                .then(handleSuccess, handleError('Error getting all actuators'));
        }

        function GetById(id) {
            return $http.get(baseUrl + '/actuators/' + id)
                .then(handleSuccess, handleError('Error getting actuator by id'));
        }

        function GetHistory(id, options) {
            var queryStr = $httpParamSerializer(options);
            return $http.get(baseUrl + '/actuators/' + id + '/history?' + queryStr)
                .then(handleSuccess, handleError('Error getting actuator history'));
        }

        function SetState(id, state) {
            var data = {
                'state': state
            }
            return $http.put(baseUrl + '/actuators/' + id, data)
                .then(handleSuccess, handleError('Error setting state'));
        }

        function SetOverride(id, override) {
            var data = {
                'override': override
            }
            return $http.put(baseUrl + '/actuators/' + id, data)
                .then(handleSuccess, handleError('Error setting override'));
        }

        function Create(actuator) {
            return $http.post(baseUrl + '/actuators', actuator)
                .then(handleSuccess, handleError('Error creating actuator'));
        }

        function Update(actuator) {
            return $http.put(baseUrl + '/actuators/' + actuator.id, actuator)
                .then(handleSuccess, handleError('Error updating actuator'));
        }

        function Delete(id) {
            return $http.delete(baseUrl + '/actuators/' + id)
                .then(handleSuccess, handleError('Error deleting actuator'));
        }

        // private functions

        function handleSuccess(res) {
            return {success: true, data: res.data};
        }

        function handleError(error) {
            return function (response) {
                throw {success: false, message: error, res: response}
            };
        }
    }

})();