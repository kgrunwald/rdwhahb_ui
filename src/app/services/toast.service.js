(function () {
    'use strict';

    angular
        .module('BlurAdmin')
        .factory('ToastService', ToastService);

    ToastService.$inject = ['toastr', 'toastrConfig'];
    function ToastService(toastr, toastrConfig) {
        angular.extend(toastrConfig, {
            "autoDismiss": true,
            "positionClass": "toast-top-right",
            "type": "info",
            "timeOut": "5000",
            "extendedTimeOut": "2000",
            "allowHtml": false,
            "closeButton": false,
            "tapToDismiss": true,
            "progressBar": false,
            "newestOnTop": true,
            "maxOpened": 3,
            "preventDuplicates": false,
            "preventOpenDuplicates": true
        });

        var service = {};

        service.Open = Open;
        service.ErrorFromResponse = ErrorFromResponse;

        return service;

        function Open(type, title, message) {
            toastr[type](message, title)
        }

        function ErrorFromResponse(response) {
            var message = ""
            if (response.res.data) {
                if (response.res.data.message instanceof Object) {
                    var keys = Object.keys(response.res.data.message);
                    for (var i in keys) {
                        message = message + response.res.data.message[keys[i]] + ', '
                    }
                    message = message.slice(0, -2)
                } else {
                    message = message + response.res.data.message
                }
            } else {
                message = 'Unknown Error'
            }
            this.Open('error', response.message, message)
        }
    }
})();