angular.module('BlurAdmin').service('modalService', ['$uibModal',
    function ($uibModal) {

        var modalDefaults = {
            type: 'info',
            backdrop: true,
            keyboard: true,
            modalFade: true,
            templateUrl: 'app/theme/components/modalTemplates/successModal.html'
        };

        var modalOptions = {
            closeButtonText: 'Close',
            actionButtonText: 'OK',
            headerText: 'Proceed?',
            bodyText: 'Perform this action?'
        };

        this.showModal = function (customModalDefaults, customModalOptions) {
            if (!customModalDefaults) customModalDefaults = {};
            customModalDefaults.backdrop = 'static';
            return this.show(customModalDefaults, customModalOptions);
        };

        this.show = function (customModalDefaults, customModalOptions) {
            //Create temp objects to work with since we're in a singleton service
            var tempModalDefaults = {};
            var tempModalOptions = {};

            //Map angular-ui modal custom defaults to modal defaults defined in service
            angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

            //Map modal.html $scope custom properties to defaults defined in service
            angular.extend(tempModalOptions, modalOptions, customModalOptions);

            if (!tempModalDefaults.controller) {
                tempModalDefaults.controller = function ($scope, $uibModalInstance, $timeout) {
                    $scope.modalOptions = tempModalOptions;

                    $scope.autoClose = function () {
                        if ($scope.modalOptions.autoClose) {
                            $timeout(function () {
                                $uibModalInstance.close();
                            }, $scope.modalOptions.autoClose);
                        }
                    }

                    $scope.autoClose();
                }
            }
            return $uibModal.open(tempModalDefaults).result;
        };

    }]);