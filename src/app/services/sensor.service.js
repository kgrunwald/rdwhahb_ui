(function () {
    'use strict';

    angular
        .module('BlurAdmin')
        .factory('SensorService', SensorService);

    SensorService.$inject = ['baseUrl', '$http', '$httpParamSerializer'];
    function SensorService(baseUrl, $http, $httpParamSerializer) {
        var service = {};

        service.GetAll = GetAll;
        service.GetById = GetById;
        service.GetHistory = GetHistory;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
        service.SetValue = SetValue;

        return service;

        function GetAll(options) {
            var queryStr = $httpParamSerializer(options);
            return $http.get(baseUrl + '/sensors?' + queryStr)
                .then(handleSuccess, handleError('Error getting all sensors'));
        }

        function GetById(id) {
            return $http.get(baseUrl + '/sensors/' + id)
                .then(handleSuccess, handleError('Error getting sensor by id'));
        }

        function GetHistory(id, options) {
            var queryStr = $httpParamSerializer(options);
            return $http.get(baseUrl + '/sensors/' + id + '/history?' + queryStr)
                .then(handleSuccess, handleError('Error getting sensor history'));
        }

        function Create(sensor) {
            return $http.post(baseUrl + '/sensors', sensor)
                .then(handleSuccess, handleError('Error creating sensor'));
        }

        function Update(sensor) {
            return $http.put(baseUrl + '/sensors/' + sensor.id, sensor)
                .then(handleSuccess, handleError('Error updating sensor'));
        }

        function Delete(id) {
            return $http.delete(baseUrl + '/sensors/' + id)
                .then(handleSuccess, handleError('Error deleting sensor'));
        }

        function SetValue(id, data) {
            return $http.post(baseUrl + '/sensors/' + id + '/value', data)
                .then(handleSuccess, handleError('Error creating sensor'));
        }

        // private functions

        function handleSuccess(res) {
            return {success: true, data: res.data};
        }

        function handleError(error) {
            return function (response) {
                throw {success: false, message: error, res: response}
            };
        }
    }

})();