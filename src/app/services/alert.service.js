(function () {
    'use strict';

    angular
        .module('BlurAdmin')
        .factory('AlertService', AlertService);

    AlertService.$inject = ['baseUrl', '$http', '$httpParamSerializer'];
    function AlertService(baseUrl, $http, $httpParamSerializer) {
        var service = {};

        service.GetAll = GetAll;
        service.GetById = GetById;
        service.Acknowledge = Acknowledge;
        service.Read = Read;

        return service;

        function GetAll(options) {
            var queryStr = $httpParamSerializer(options);
            return $http.get(baseUrl + '/health/alerts?' + queryStr)
                .then(handleSuccess, handleError('Error getting all alerts'));
        }

        function GetById(id) {
            return $http.get(baseUrl + '/health/alerts/' + id)
                .then(handleSuccess, handleError('Error getting alert by id'));
        }

        function Acknowledge(id) {
            var data = {
                alertState: 'ACKNOWLEDGED'
            }
            return $http.put(baseUrl + '/health/alerts/' + id, data)
                .then(handleSuccess, handleError('Error acknowledging alert'));
        }

        function Read(id, read) {
            var data = {
                read: read
            }
            return $http.put(baseUrl + '/health/alerts/' + id, data)
                .then(handleSuccess, handleError('Error acknowledging alert'));
        }

        // private functions

        function handleSuccess(res) {
            return {success: true, data: res.data};
        }

        function handleError(error) {
            return function (response) {
                throw {success: false, message: error, res: response}
            };
        }
    }

})();