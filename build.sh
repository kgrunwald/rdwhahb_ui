# Make sure the user you use to run this script has write access to /var/www
# (add user to root group)

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR
git reset --hard HEAD
git pull
mkdir -p /var/www/
ln -sf `pwd ./` /var/www/
npm install bower
npm install
bower install
./node_modules/gulp/bin/gulp.js build
